﻿using Microsoft.AspNetCore.SignalR;

namespace SignalR.Web.Hubs
{
    public class DeathlyHallowsHub : Hub
    {
        public Dictionary<string, int> GetRaceStatus()
        {
            return StaticDetails.DeathlyHallowRace;
        }
    }
}
