﻿namespace SignalR.Web.Models.ViewModels
{
    public class ChatVM
    {
        public int MaxRoomAllowed { get; set; }
        public IList<ChatRoom> Rooms { get; set; }
        public string? UserId { get; set; }
        public bool AllowAddRoom => Rooms == null || Rooms.Count < MaxRoomAllowed;

        public ChatVM()
        {
            Rooms = new List<ChatRoom>();       
        }
    }
}
